<form action="index.php?page=danhmuc&act=them" method="post" enctype="multipart/form-data" name="form1" id="form1">
  <table align="center" border="0" width="100%">
    <tbody>
      <tr>
        <td class="title" align="center" width="100%">Thêm mới danh mục </td>
      </tr>
    </tbody>
  </table>
  <table align="center" cellpadding="0" cellspacing="0" width="100%">
    <tbody>
      <tr>
        <td class="fr">Thuộc thể loại</td>
        <td class="fr_2"><label>
          <select name="idtl" id="idtl" validate="required:true">
            <option>Thuộc thể loại</option>
            <option value="1">Bài viết</option>
            <option value="2">Tin tức</option>
            <option value="3">Sản phẩm</option>
          </select>
        </label></td>
      </tr>
      <tr>
        <td width="200" class="fr">Tên danh mục</td>
        <td class="fr_2"><label>
          <input name="tendm" type="text" id="tendm" value="<?php if(isset($_POST['tendm'])){ echo $_POST['tendm'];}?>" size="35" validate="required:true"/>
        </label></td>
      </tr>
        <tr>
          <td class="fr">Tóm tắt</td>
          <td class="fr_2"><label>
            <textarea name="tomtat" id="tomtat" cols="45" rows="5" validate="required:true"></textarea>
          </label></td>
        </tr>
        <tr>
          <td class="fr">Keyword</td>
          <td class="fr_2"><label>
            <textarea name="keyword" id="keyword" cols="45" rows="3" validate="required:true"></textarea>
          </label></td>
        </tr>
        <tr>
        <td class="fr">Thứ tự</td>
        <td class="fr_2"><input name="thutu" type="text" id="thutu" value="<?php if(isset($_POST['thutu'])){ echo $_POST['thutu'];}?>" validate="required:true,number:true"/></td>
      </tr>
      <tr>
        <td class="fr">Trạng thái</td>
        <td class="fr_2"><select name="trangthai" id="trangthai" validate="required:true">
          <option value="">Trạng thái&nbsp;&nbsp;</option>
          <option value="1">Có</option>
          <option value="0">Không </option>
        </select></td>
      </tr>
      <tr>
        <td></td>
        <td align="left"><div id="wait"></div>
              <div class="buttons">
    <button type="submit" class="positive" name="them">
        <img src="images/apply2.png" alt=""/> 
        Cập nhập
    </button>


    <button type="reset" class="positive" name="reset">
        <img src="images/cross.png" alt=""/>
        Làm lại
    </button>
</div></td>
      </tr>
     
    </tbody>
  </table>

</form>
