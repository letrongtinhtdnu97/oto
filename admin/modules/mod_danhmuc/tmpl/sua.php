<form action="index.php?page=danhmuc&act=sua&id=<?php echo $mang[0];?>" method="post" enctype="multipart/form-data" name="form1" id="form1">
  <table align="center" border="0" width="100%">
    <tbody>
      <tr>
        <td class="title" align="center" width="100%">Cập nhập danh mục</td>
      </tr>
    </tbody>
  </table>
  <table align="center" cellpadding="0" cellspacing="0" width="100%">
    <tbody>
      <tr>
        <td class="fr">Thuộc thể loại</td>
        <td class="fr_2"><label>
          <select name="idtl" id="idtl">
            <option>Thuộc thể loại</option>
            <option value="1" <?php if($mang[1]==1){?> selected="selected"<?php }?>>Bài viết</option>
            <option value="2" <?php if($mang[1]==2){?> selected="selected"<?php }?>>Tin tức</option>
            <option value="3" <?php if($mang[1]==3){?> selected="selected"<?php }?>>Sản phẩm</option>
          </select>
        </label></td>
      </tr>
      <tr>
        <td width="200" class="fr">Tên danh mục</td>
        <td class="fr_2"><label>
          <input name="tendm" type="text" id="tendm" value="<?php echo $mang[2];?>" size="45" validate="required:true"/>
        </label></td>
      </tr>
      <tr>
        <td class="fr">Tóm tắt</td>
        <td class="fr_2"><textarea name="tomtat" id="tomtat" cols="45" rows="5" validate="required:true"><?php echo $mang[5];?></textarea></td>
      </tr>
      <tr>
        <td class="fr">Keyword</td>
        <td class="fr_2"><textarea name="keyword" id="keyword" cols="45" rows="3" validate="required:true"><?php echo $mang[6];?></textarea></td>
      </tr>
      <tr>
        <td class="fr">Thứ tự</td>
        <td class="fr_2"><input name="thutu" type="text" id="thutu" value="<?php echo $mang[3];?>" validate="required:true,number:true"/></td>
      </tr>
      <tr>
        <td class="fr">Trạng thái</td>
        <td class="fr_2"><select name="trangthai" id="trangthai" validate="required:true">
         <option value="">Trạng thái&nbsp;&nbsp;</option>
        <option value="1"<?php if($mang[4]==1){?> selected="selected"<?php }?>>Có</option>
        <option value="0"<?php if($mang[4]==0){?> selected="selected"<?php }?>>Không </option>
        </select></td>
      </tr>
      <tr>
        <td></td>
        <td align="left"><div id="wait"></div>
             <div class="buttons">
    <button type="submit" class="positive" name="capnhap">
        <img src="images/apply2.png" alt=""/> 
        Cập nhập
    </button>


    <button type="reset" class="positive" name="reset">
        <img src="images/cross.png" alt=""/>
        Làm lại
    </button>
</div></td>
      </tr>
    </tbody>
  </table>
</form>
