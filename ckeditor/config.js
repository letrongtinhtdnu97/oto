﻿/*
Copyright (c) 2003-2012, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.html or http://ckeditor.com/license
*/

CKEDITOR.editorConfig = function( config )
{
// Define changes to default configuration here. For example:
// config.language = 'fr';
// config.uiColor = '#AADC6E';

config.filebrowserBrowseUrl = 'http://nissangiaiphong.com/ckfinder/ckfinder.html';

config.filebrowserImageBrowseUrl = 'http://nissangiaiphong.com/ckfinder/ckfinder.html?type=Images';

config.filebrowserFlashBrowseUrl = 'http://nissangiaiphong.com/ckfinder/ckfinder.html?type=Flash';

config.filebrowserUploadUrl = 'http://nissangiaiphong.com/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files';

config.filebrowserImageUploadUrl = 'http://nissangiaiphong.com/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images';

config.filebrowserFlashUploadUrl = 'http://nissangiaiphong.com/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash';
};