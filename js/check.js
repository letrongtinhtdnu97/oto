// JavaScript Document

function checkEmail()
{
	email = $('#txtSend');
	if(!isValidEmail(email[0].value))
	{
		alert('Bạn nhập thiếu dữ liệu hoặc chưa đúng!');
		return false;
	}
	else
	{
		return true;
	}
}

function isValidEmail(emailAddress)
{
	var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
	return pattern.test(emailAddress);
}

function repText(elem)
{
	pt = $(elem);
	if(pt[0].style.color == 'gray' && pt[0].value == pt[0].title)
	{
		pt[0].value = "";
		pt[0].style.color = 'black';
	}
}

function fillText(elem)
{
	pt = $(elem);
	if(pt[0].value == "")
	{
		pt[0].value = pt[0].title;
		pt[0].style.color = 'gray';
	}
}


function repMenu(id)
{
	lis = $('#' + id + ' ul').html();
	$('#support_menu').html(lis);
}
